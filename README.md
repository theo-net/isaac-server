Abraham server config
=====================

We use Ansible (https://www.ansible.com/) to deploy our personal server. It's use 
for our NAS.

The script are make for an *Ubuntu 22.04 (Jammy Jellyfish)* system.


## Vagrant ##

For local tests, you can use Vagrant with the file `Vagrantfile` in the root 
repository. 

In Ubuntu, execute:

    sudo apt install vagrant virtualbox virtualbox-dkms

You need to correct the IP address in the configuration file. The address need to
not be in an existing network (the default will be good). Check if you have an 
public key in `~/.ssh/id_rsa.pub`. If not, you need to create one or correct the
path in the environment variable or in the configuration file. Finally, adapt the 
memory size and the cpu number available. Then : 

    SSH_KEY=~/.ssh/id_rsa.pub vagrant up

For the SSH connection to the virtual system: 

    SSH_KEY=~/.ssh/id_rsa.pub vagrant ssh


## Real system ##

If you use Vagrant, you don't need to do nothing. You can use the user `vagrant` 
without password.

On real system, you need to configure the hostname with `/etc/hostname` and 
`/etc/hosts`.
 
Add an sudo user if you don't have:

    sudo adduser --ingroup sudo myUsername

Then, install Pyton:

    sudo apt install python-is-python3

If you want RAID, configure it now!


### Ansible Lint ###

For installation:

    pip3 install "ansible-lint[yamllint]"

To use : 

    ansible-lint test.yml


### Securization of SSH ###

To send your public key:

    ssh-copy-id -i ~/.ssh/id_rsa.pub myUsername@server.domain.com

It's good to use an other port for the SSH server. You can define it in `/etc/ssh/sshd_config` :

    Port 1234

If you want use the password authentication:

    PasswordAuthentication yes

Then:

    sudo service ssh restart

In your local `~/.ssh/config` add this block:

    Host server.domain.com
    Port 1234
    User myUsername

### Git configuration ###

Ansible install git, but if you need to use private repositories, you need to configure it before.

    sudo apt install git
    git config --global user.name "John Doe"
    git config --global user.email johndoe@example.com

Then, generate a key (choose `~/.ssh/gitlab` for the path):

    ssh-keygen -t ed25519 -C "email@example.com"

Configure your ssh connections list in `~/.ssh/config`:

    Host gitlab.com
    Preferredauthentications publickey
    IdentityFile ~/.ssh/gitlab

And add the content of `~/.ssh/gitlab.pub` in your personal keys list for GitLab (https://gitlab.com/-/profile/keys).

To test the configuration, use:

    ssh -T git@gitlab.com


### Ansible ###

In the host, for the installation, use:

    sudo apt install ansible

### Don't forget ###

To connect Telegraf with fail2ban, add the line:

```
Cmnd_Alias FAIL2BAN = /usr/bin/fail2ban-client status, /usr/bin/fail2ban-client status *
telegraf  ALL=(root) NOEXEC: NOPASSWD: FAIL2BAN,/usr/sbin/smartctl
Defaults!FAIL2BAN !logfile, !syslog, !pam_session
```

with:

    sudo visudo -f /etc/sudoers.d/telegraf

**An error and sudo not will work... you need to use a physical screen and pass the system in recovery mode***

Then, restart `telegraf` service


Start NodeJs app:

    sudo -u pm2 pm2 start server.js --name tn_network -- start

Enable PM2 on startup:

    sudo -u pm2 pm2 startup
    sudo -u pm2 pm2 save
    sudo env PATH=$PATH:/usr/bin /usr/lib/node_modules/pm2/bin/pm2 startup systemd -u pm2 --hp /home/pm2

If you want to add new application on startup, don't forget: 

    sudo -u pm2 pm2 save


For backup, some users need to be in the `backup_user` group (the name depends on your configuration) :

    sudo adduser [username] [group]


### Certbot with Gandi plugin ###

Documentation available in https://github.com/obynio/certbot-plugin-gandi

### Final tests ###

First, check the UFW's configuration with:

    sudo ufw status

then for fail2ban:

    sudo fail2ban-client status

Finally, for the mail configuration, you can send an test mail:

    echo 'message' | msmtp your-address@email.com
    echo 'message' | mail your-address@email.com

If Telegraf doesn't start, check the configuration with:

    telegraf -test -config /etc/telegraf/telegraf.conf

#!/bin/bash
LANG=C
WAIT_TIMER=1
SCRIPT_PID=${$}
SCRIPT_NAME=$(basename $0)
SCRIPT_PATH=$(dirname $0)

SCRIPT_PARAMETERS_NUMBERS=$#
SCRIPT_START_DATE=$(date +"%Y-%m-%d-%H:%M")
SCRIPT_START_TIME=$(date +"%H:%M")
SCRIPT_DATE=$(date +"%Y%m%d_%H%M")

backupuser="www-data"

app_name="oasis"
app_db_name="{{ oasis_db_name }}"
app_db_credential_file='{{ backup_dir }}/{{ ansible_hostname }}/.credentials/mysql/{{ oasis_db_user }}@localhost.cred'

dump_directory="{{ backup_dir }}/{{ ansible_hostname }}/$app_name"
db_dump_file_name="$app_db_name-$SCRIPT_DATE.dump"
dump_file_directory="$dump_directory/last"

mysqldump_options=""
mysqldump_options="$mysqldump --routines"
mysqldump_options="$mysqldump --events"
mysqldump_options="$mysqldump --lock-tables"
mysqldump_options="$mysqldump --single-transaction"
mysqldump_options="$mysqldump --master-data=2"

_PublicKey='{{ backup_dir }}/{{ ansible_hostname }}/.credentials/backup.pub.pem'

declare -rA app_dump_directories=(['/var/www/oasis/public/upload/']="$dump_file_directory/upload" \
                                   ) 

#
#
# Script principal - MAIN()
_fname="main()"
echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - START : $SCRIPT_NAME"
echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - START in ($SCRIPT_PATH)"
echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - START with ($SCRIPT_PARAMETERS_NUMBERS) parameters"
echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - START on ($SCRIPT_START_DATE) at ($SCRIPT_START_TIME)"
cd "$SCRIPT_PATH"

#
## Create dump directory
_task="Create dump directory"
_cmd="mkdir -p $dump_file_directory"
echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - Start"
echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - Commande = \"$_cmd\""
_retval=$($_cmd 2>&1 )
_retcode=$?
if [ "$_retcode" -ne 0 ] ; then
  echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - Commande faild !"
  echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - Commande = \"$_cmd\""
  echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - return ($_retcode)"
  $(echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - Commande faild !"  >&2)
  $(echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - $_retval" >&2)
  exit "$_retcode"
else
  echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - End"
fi
##
#

#
## Dump MySql Database
_task="Dump MySql Database"
_cmd="mysqldump $mysqldump --defaults-file=$app_db_credential_file --databases $app_db_name --result-file=$dump_directory/$db_dump_file_name --log-error=$dump_directory/$db_dump_file_name.errors"
echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - Start"
echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - Commande = \"$_cmd\""
_retval=$($_cmd 2>&1 )
_retcode=$?
if [ "$_retcode" -ne 0 ] ; then
  echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - Commande faild !"
  echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - Commande = \"$_cmd\""
  echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - return ($_retcode)"
  $(echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - Commande faild !"  >&2)
  $(echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - $_retval" >&2)
  exit "$_retcode"
else
  echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - End"
fi
##
#

#
## TASK : Sync application files - START
_task="Sync application files"
echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - Start"
for _source_directory in ${!app_dump_directories[@]}
do
  echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - Source Directory : $_source_directory"
  echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - Target Directory : ${app_dump_directories[$_source_directory]}"
  _cmd="rsync -av --delete-after $_source_directory ${app_dump_directories[$_source_directory]}"
  echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - Commande = \"$_cmd\""
  _retval=$($_cmd 2>&1 )
  _retcode=$?
  if [ "$_retcode" -ne 0 ] ; then
    echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - Commande faild !"
    echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - Commande = \"$_cmd\""
    echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - return ($_retcode)"
    $(echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - Commande faild !"  >&2)
    $(echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - $_retval" >&2)
  fi
done
echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - End"
##
#

#
## Tar & Compress Application Files
_task="Tar & Compress Application Files"

_cmd="tar -cvzf $dump_directory/$app_name-$SCRIPT_DATE-FilesBackup.tar.gz -C $dump_file_directory ."
echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - Start"
echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - Commande = \"$_cmd\""
_retval=$($_cmd 2>&1 )
_retcode=$?
if [ "$_retcode" -ne 0 ] ; then
  echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - Commande faild !"
  echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - Commande = \"$_cmd\""
  echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - return ($_retcode)"
  $(echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - Commande faild !"  >&2)
  $(echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - $_retval" >&2)
  exit "$_retcode"
else
  echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - End"
fi
##
#

#
## Compress Database Dump File
_task="Compress DB Dump File"
_cmd="gzip $dump_directory/$db_dump_file_name"
echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - Start"
echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - Commande = \"$_cmd\""
_retval=$($_cmd 2>&1 )
_retcode=$?
if [ "$_retcode" -ne 0 ] ; then
  echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - Commande faild !"
  echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - Commande = \"$_cmd\""
  echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - return ($_retcode)"
  $(echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - Commande faild !"  >&2)
  $(echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - $_retval" >&2)
  exit "$_retcode"
else
  echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - End"
fi
##
#

#
## Générate passphrase
_task="Générate passphrase"
_cmd="openssl rand -out $dump_directory/$db_dump_file_name.password.key -base64 124"
echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - Start"
echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - Commande = \"$_cmd\""
_retval=$($_cmd 2>&1 )
_retcode=$?
if [ "$_retcode" -ne 0 ] ; then
  echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - Commande faild !"
  echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - Commande = \"$_cmd\""
  echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - return ($_retcode)"
  $(echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - Commande faild !"  >&2)
  $(echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - $_retval" >&2)
  exit "$_retcode"
else
  echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - End"
fi
##
#

#
## Encrypt DB Dump
_task="Encrypt DB Dump"
_cmd="openssl enc -pbkdf2 -a -salt -in $dump_directory/$db_dump_file_name.gz -out $dump_directory/$db_dump_file_name.gz.crypt -pass file:$dump_directory/$db_dump_file_name.password.key"
echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - Start"
echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - Commande = \"$_cmd\""
_retval=$($_cmd 2>&1 )
_retcode=$?
if [ "$_retcode" -ne 0 ] ; then
  echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - Commande faild !"
  echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - Commande = \"$_cmd\""
  echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - return ($_retcode)"
  $(echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - Commande faild !"  >&2)
  $(echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - $_retval" >&2)
  exit "$_retcode"
else
  echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - End"
fi
##
#

#
## Encrypt Passphrase
_task="Encrypt Passphrase"
_cmd="openssl rsautl -encrypt -pubin -inkey $_PublicKey -in $dump_directory/$db_dump_file_name.password.key -out $dump_directory/$db_dump_file_name.password.key.crypt"
echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - Start"
echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - Commande = \"$_cmd\""
_retval=$($_cmd 2>&1 )
_retcode=$?
if [ "$_retcode" -ne 0 ] ; then
  echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - Commande faild !"
  echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - Commande = \"$_cmd\""
  echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - return ($_retcode)"
  $(echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - Commande faild !"  >&2)
  $(echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - $_retval" >&2)
  exit "$_retcode"
else
  echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - End"
fi
##
#

#
## Tar & Compress Application Files
_task="Create Application TarBall"
_cmd="tar -cvf $dump_directory/$db_dump_file_name.tar \
                          -C $dump_directory $db_dump_file_name.password.key.crypt \
                          -C $dump_directory $db_dump_file_name.errors \
                          -C $dump_directory $db_dump_file_name.gz.crypt"
echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - Start"
echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - Commande = \"$_cmd\""
_retval=$($_cmd 2>&1 )
_retcode=$?
if [ "$_retcode" -ne 0 ] ; then
  echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - Commande faild !"
  echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - Commande = \"$_cmd\""
  echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - return ($_retcode)"
  $(echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - Commande faild !"  >&2)
  $(echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - $_retval" >&2)
  exit "$_retcode"
else
  echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - End"
fi
##
#

#
## Change group owner
_task="Change group owner"
_cmd="chown :{{ backup_user }} $dump_directory/$db_dump_file_name.tar"
echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - Start"
echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - Commande = \"$_cmd\""
_retval=$($_cmd 2>&1 )
_retcode=$?
if [ "$_retcode" -ne 0 ] ; then
  echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - Commande faild !"
  echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - Commande = \"$_cmd\""
  echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - return ($_retcode)"
  $(echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - Commande faild !"  >&2)
  $(echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - $_retval" >&2)
  exit "$_retcode"
else
  echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - End"
fi
##
#

#
## Remove passphrase & dump file & tarball
_task="Remove passphrase & dump file & tarball"
_cmd="rm -rf $dump_directory/$db_dump_file_name.password.key \\
             $dump_directory/$db_dump_file_name \\
             $dump_directory/$db_dump_file_name.gz \\
             $dump_directory/$db_dump_file_name.gz.crypt \\
             $dump_directory/$db_dump_file_name.errors \\
             $dump_directory/$db_dump_file_name.password.key.crypt \\
             "
echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - Start"
echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - Commande = \"$_cmd\""
_retval=$($_cmd 2>&1 )
_retcode=$?
if [ "$_retcode" -ne 0 ] ; then
  echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - Commande faild !"
  echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - Commande = \"$_cmd\""
  echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - return ($_retcode)"
  $(echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - Commande faild !"  >&2)
  $(echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - $_retval" >&2)
  exit "$_retcode"
else
  echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - End"
fi
##
#

#
## Remove older dump files
_task="Remove older dump files"
_cmd="rm -rf $dump_directory/$app_db_name-`date --date='yesterday' +'%Y%m%d'`_* "
echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - Start"
echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - Commande = \"$_cmd\""
_retval=$($_cmd 2>&1 )
_retcode=$?
if [ "$_retcode" -ne 0 ] ; then
  echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - Commande faild !"
  echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - Commande = \"$_cmd\""
  echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - return ($_retcode)"
  $(echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - Commande faild !"  >&2)
  $(echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - $_retval" >&2)
  exit "$_retcode"
else
  echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - End"
fi
##
#

#
## Remove older application files
_task="Remove older application files"
_cmd="rm -rf $dump_directory/$app_name-`date --date='yesterday' +'%Y%m%d'`_* "
echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - Start"
echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - Commande = \"$_cmd\""
_retval=$($_cmd 2>&1 )
_retcode=$?
if [ "$_retcode" -ne 0 ] ; then
  echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - Commande faild !"
  echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - Commande = \"$_cmd\""
  echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - return ($_retcode)"
  $(echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - Commande faild !"  >&2)
  $(echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - [ERROR] - $_retval" >&2)
  exit "$_retcode"
else
  echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - $_fname - $_task - End"
fi
##
#

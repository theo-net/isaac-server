#!/bin/bash
LANG=C
WAIT_TIMER=1
SCRIPT_PID=${$}
SCRIPT_NAME=$(basename $0)
SCRIPT_PATH=$(dirname $0)
SCRIPT_PARAMETERS_NUMBERS=$#
SCRIPT_START_DATE=$(date +"%Y-%m-%d-%H:%M")
SCRIPT_START_TIME=$(date +"%H:%M")
LOG_DIR='/var/log/scripts'

ReturnValue=0


cd $SCRIPT_PATH

#Backup Oasis
application="Oasis"
echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - START : $application"
retcode=0
$(./www-data@backup_oasis_application.sh \
        >> "$LOG_DIR/$SCRIPT_NAME.log" \
        2>&1)
retcode=$?
if [ "$retcode" -ne 0 ] ; then
  $(echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - [ERROR] - Backup $application faild ! ($retcode)"  >&2)
fi
echo $retcode > "$LOG_DIR/$SCRIPT_NAME.end"
ReturnValue=$(($ReturnValue+$retcode))
echo "$(date +"%s")" > "$LOG_DIR/$SCRIPT_NAME.run"
echo "$(date) - [$SCRIPT_NAME ($SCRIPT_PID)] - End ($retcode) : Backup $application"

exit $ReturnValue

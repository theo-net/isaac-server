#!/bin/bash
LANG=C
WAIT_TIMER=1
SCRIPT_PID=${$}
SCRIPT_NAME=$(basename $0)
SCRIPT_PATH=$(dirname $0)
SCRIPT_PARAMETERS_NUMBERS=$#
SCRIPT_START_DATE=$(date +"%Y-%m-%d-%H:%M")
SCRIPT_START_TIME=$(date +"%H:%M")
LOG_DIR='/var/log/scripts'

cd $SCRIPT_PATH

$(./backup_ldap.sh \
        >> "$LOG_DIR/$SCRIPT_NAME.log" \
        2>&1)
_retcode=$?

echo $_retcode > "$LOG_DIR/$SCRIPT_NAME.end"


DATE=$(date +"%s")
echo $DATE > "$LOG_DIR/$SCRIPT_NAME.run"
exit $_retcode
